import express from 'express';
import path from 'path';

let app = express();

app.use('/scripts', express.static(path.join(__dirname, '../node_modules/bootstrap/dist')));
app.use('/jquery', express.static(path.join(__dirname, '../node_modules/jquery/dist/')));
app.use('/fa', express.static(path.join(__dirname, '../node_modules/font-awesome')));
app.use('/cropper', express.static(path.join(__dirname, '../node_modules/cropperjs/dist')));
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});


app.get('/*', (req, res) => {
   // res.send('hello react');
    res.sendFile(path.join(__dirname, '../index.html'));
});

const port = 3000;
app.listen(3000, () => console.log(`Server Started at localhost:${port}`));
